using AutoMapper;
using Gfi_ConceptTest_CRUD2.Entities;
using Gfi_ConceptTest_CRUD2.Models;
using Gfi_ConceptTest_CRUD2.Repository;
using Gfi_ConceptTest_CRUD2.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.IO;

namespace Gfi_ConceptTest_CRUD2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddAutoMapper(configuration => 
            {
                configuration.CreateMap<Author, AuthorDTO>();
                configuration.CreateMap<AuthorDTO, Author>();
            }, typeof(Startup));

            services.AddScoped<IAuthorRepository, AuthorRepository>();
            services.AddScoped<AuthorService>();

            CreateAuthorsRepository();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile("Logs/Gfi_ConceptTest_CRUD2-{Date}.txt");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void CreateAuthorsRepository() 
        {
            var authorsFile = Configuration.GetValue<string>("AuthorsFile");
            if (!File.Exists(authorsFile))
            {
                File.Create(authorsFile);
            }
        }
    }
}
