﻿using Newtonsoft.Json.Converters;

namespace Gfi_ConceptTest_CRUD2.Services
{
    public class IsoDateConverter : IsoDateTimeConverter
    {
        public IsoDateConverter() =>
            this.DateTimeFormat = Culture.DateTimeFormat.ShortDatePattern;
    }
}
