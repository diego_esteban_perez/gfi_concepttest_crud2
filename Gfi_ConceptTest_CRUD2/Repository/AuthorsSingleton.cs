﻿using Gfi_ConceptTest_CRUD2.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Gfi_ConceptTest_CRUD2.Repository
{
    public class AuthorsSingleton
    {
        private static AuthorsSingleton instance;
        public List<Author> authors;

        private AuthorsSingleton()
        {
            try
            {
                authors = new List<Author>();
            }
            catch { }
        }

        public static AuthorsSingleton GetInstance
        {
            get
            {
                if (instance == null)
                    instance = new AuthorsSingleton();
                return instance;
            }
        }
    }
}
