﻿using Gfi_ConceptTest_CRUD2.Entities;
using Gfi_ConceptTest_CRUD2.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gfi_ConceptTest_CRUD2.Repository
{
    public interface IAuthorRepository
    {
        Author Insert(AuthorDTO authorDTO);
        Author GetAuthor(int id);
        List<Author> GetAuthors();
        int Modify(int id, AuthorDTO author);
        int Delete(int id);
    }
}
