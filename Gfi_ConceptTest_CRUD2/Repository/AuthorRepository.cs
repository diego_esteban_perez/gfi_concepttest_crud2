﻿using AutoMapper;
using Gfi_ConceptTest_CRUD2.Entities;
using Gfi_ConceptTest_CRUD2.Models;
using System.Collections.Generic;
using System.Linq;

namespace Gfi_ConceptTest_CRUD2.Repository
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly IMapper mapper;
        private AuthorsSingleton authorsSingleton;

        public AuthorRepository(IMapper mapper)
        {
            this.mapper = mapper;
            authorsSingleton = AuthorsSingleton.GetInstance;
        }

        public Author GetAuthor(int id)
        {
            return authorsSingleton.authors.Where(x => x.Id == id).FirstOrDefault();
        }

        public List<Author> GetAuthors()
        {
            return authorsSingleton.authors;
        }

        public Author Insert(AuthorDTO authorDTO)
        {
            int id = 1;
            if (authorsSingleton.authors.Count() > 0) {
                id = authorsSingleton.authors.OrderBy(x => x.Id).Last().Id + 1;
            }
            
            Author author = mapper.Map<Author>(authorDTO);
            author.Id = id;
            authorsSingleton.authors.Add(author);

            return author;
        }

        public int Modify(int id, AuthorDTO authorDTO)
        {
            int index = authorsSingleton.authors.FindIndex(x => x.Id == id);
            if (index != -1)
            {
                authorsSingleton.authors[index] = mapper.Map<Author>(authorDTO);
            }

            return index;
        }

        public int Delete(int id)
        {
            Author author = authorsSingleton.authors.Where(x => x.Id == id).FirstOrDefault();
            if (author == null) {
                return -1;
            }
            authorsSingleton.authors.Remove(author);
            
            return id;
        }
    }
}
