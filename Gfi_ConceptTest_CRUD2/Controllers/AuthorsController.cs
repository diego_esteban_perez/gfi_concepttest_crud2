﻿using System.Collections.Generic;
using System.Threading.Tasks;

using AutoMapper;

using Gfi_ConceptTest_CRUD2.Entities;
using Gfi_ConceptTest_CRUD2.Models;
using Gfi_ConceptTest_CRUD2.Repository;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Gfi_ConceptTest_CRUD2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        private readonly IAuthorRepository authorRepository;

        public AuthorsController(IConfiguration configuration, IMapper mapper, IAuthorRepository authorRepository)
        {
            this.configuration = configuration;
            this.mapper = mapper;
            this.authorRepository = authorRepository;
        }
        // GET: api/Authors
        [HttpGet]
        public ActionResult<IEnumerable<AuthorDTO>> Get()
        {
            List<Author> authors = authorRepository.GetAuthors();
            return mapper.Map<List<AuthorDTO>>(authors);
        }

        // GET: api/Authors/5
        [HttpGet("{id}", Name = "GetAuthor")]
        public ActionResult<AuthorDTO> Get(int id)
        {
            Author author = authorRepository.GetAuthor(id);

            if (author == null)
            {
                return NotFound();
            }

            var autorDTO = mapper.Map<AuthorDTO>(author);

            return autorDTO;
        }

        // POST: api/Authors
        [HttpPost]
        public ActionResult Post([FromBody] AuthorDTO authorDTO)
        {
            Author author = authorRepository.Insert(authorDTO);
            return new CreatedAtRouteResult("GetAuthor", new { id = author.Id }, author);
        }

        // PUT: api/Authors/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] AuthorDTO author)
        {
            if (authorRepository.Modify(id, author) == -1)
            {
                return NotFound();
            }

            return Ok();
        }

        // DELETE: api/Authors/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (authorRepository.Delete(id) == -1)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
