#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 8080
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["Gfi_ConceptTest_CRUD2/Gfi_ConceptTest_CRUD2.csproj", "Gfi_ConceptTest_CRUD2/"]
RUN dotnet restore "Gfi_ConceptTest_CRUD2/Gfi_ConceptTest_CRUD2.csproj"
COPY . .
WORKDIR "/src/Gfi_ConceptTest_CRUD2"
RUN dotnet build "Gfi_ConceptTest_CRUD2.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Gfi_ConceptTest_CRUD2.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Gfi_ConceptTest_CRUD2.dll"]